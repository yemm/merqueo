package com.merqueo.app.utils

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

fun Activity.toastShort(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
//esta funcion la usamos para los mensajes toast largo
fun Activity.toastLong(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}
//esta funcion la usamos para inflar vistas en nuestras vistas recicladas
//en la función hacemos que resiba un entero para pasarle el id de la vista (layout)
fun ViewGroup.inflate(layoudId: Int): View {
    return LayoutInflater.from(context).inflate(layoudId, this, false)
}